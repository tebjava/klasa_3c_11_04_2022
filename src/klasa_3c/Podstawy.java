/*
 * Poni�sza linijka wskazuje maszynie Java, �e opracowywany plik kodu zr�d�owego nale�y do okre�lonej "paczki" kod�w-klas.
 * Taka paczka pozwalaplikom klas operowa� mi�dzy sob� okre�lonymi zasobami (sk�adowymi) bez dodatkokowych ogranicze� 
 * (dla nas na razie ma�o istotne, b�dzie wyja�niane szczeg�owow na p�niejszych zaj�ciach) 
 */
package klasa_3c;

import java.util.Scanner;

import javax.print.DocFlavor.BYTE_ARRAY;

// poni�ej nazwa klasy zawartej w pliku. Nale�y zauwa�y�, �e klasa ma identyczn� nazw� jak plik. Nie jest to przypadek
//to jeden ze standadr�w w j�zyku Java m�wi�cy - 1 plik to jedna klasa z identyczn� nazw� klasy i pliku
public class Podstawy {
	//poni�sza funkcja jest krytyczna dla funkcjonowania naszej aplikacji je�eli chcemy by mo�liwe by�o
	//jej uruchomienie. Funkcj� t� musi posiada� tylko jeden plik naszego projektu; je�eli funkcja ta wyst�puje w wielu
	//plikach to u�ywana jest ta, kt�ra jest g��wnym pliku projketu (najcz�ciej pierwsza paczka, pierszy plik z funkcj�)
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//zmienne w programowaniu to jeden z najbardziej podstawowych element�w.
		//pozwalaj� one na przechowywanie wartosci, ktore nie zostana pierwotnie przewdziane przez programiste.
		//generalnie mozemy tworzy zmienne o dowolnych nazwach - takich jakie sobie wybierzemy. Wazne jest
		//by zmienne nie zaczynaly sie od cyfry, nie posiadaly znakow specjlanych (poza _) oraz nie jest wskazane
		//by zawieraly znaki narodowe. Pnadto w Java nazwy zmiennych nie mog� kolidowa� ze slowami zastrzezonymi
		//w jezyku.
		
		//zmienne ogolne. Zmienne te wystepuja niemal w kazdym jezyku programowania.
		
		//zmienna podstawowa; pozwala na przechowanie dokladnie jednego znaku. Zmienna ma rozmiar 1 bajtu
		char znak = 'A';
		//zmienna logiczna; pozwala na przechowanie jednego z dw�ch stanow - prawdy (true) badz falszu (false)
		//ma rozmiar 1 bajt
		boolean logika = false;
		//zmienna przechowujaca wartosci liczbowe calkowite (ze znakiem). Zajmuje 2 bajty.
		short krotka = 134; 
		//zmienna liczbowa calkowita (ze znakiem). Zajmuje 4 bajty.
		int calkowita = 787644;
		//zmienna liczbowa calkowita (ze znakiem). Zajmuje 8 bajtow.
		long dluga = 733244445;
		//zmienna zmiennoprzecinkowa pojedynczej precyzji. 
		float zmienna = 1.5676f;
		//zmienna zmiennoprzecinkowa podwojnej precyzji.
		double powd_zmienna = 1.78;
		//zmienna pozwala na przechowywanie potencjalnie niegoraniczonego niczym ci�gu znakowego.
		String ciag_znakowy = "Jest super";
		
		//zmienne charakterystyczne dla Java
		
		//zmienna bajtowa; pozwala na zpis pojedynczego bajtu dowolnej wartosci (znaku lub wartosci
		//liczbowej); stosowana do tzw. trybu surowego (wartosc bez obrobki)
		byte bajt = 67;
		
		final int liczbas = 89;
		
		calkowita = 10;
		
		//liczbas = 89;
		
		//Poni�szy kod to kod prezentuj�cy dzia�anie print/println oraz 
		//klasy Scanner
//		System.out.print("Witaj w �wiecie programist�w (na razie ograniczony zakres, ale b�dzie fajnie)!");
//		System.out.println("To kolejna linia tekstu");
//		System.out.println("Wartosc float: " + zmienna);
//		System.out.print("Podaj swoje imie: ");
//		Scanner sc = new Scanner(System.in);
//		ciag_znakowy = sc.next();
//		System.out.println("Twoje imie to: " + ciag_znakowy);
		
		/*
		 * Poni�szy kod ma na celu pokazanie dost�pnych w Java operator�w
		 * oraz przyk�adowego ich u�ycia
		 */
		
		//operatory arytmetyczne:
		// + - operator dodaj�cy dwa argumenty (warto�ci) liczbowe lub tekstowe (��czenie tekstu) 
		// - - operator r�nicy - odejmuje jeden argument od drugiego
		// * - operator mno�enia; pozwala na przemno�enie dw�ch warto�ci
		// / - operator dzielenia. Dzieli jedn� warto�� przez drug� (ale nie przez zero)
		// % - operator modulo; pozyskuje warto�� reszty z dzielenia pierwszego argumentu z dzielenia przez drugi argument
		
		//przyk�ady (mo�liwo�� operacji zar�wno na litera�ach jak i warto�ciach zapisanych pod zmiennymi)
		int dodaj = 7+8;
		int odejmij = 67-dodaj;
		int mnoz = 8*dodaj;
		int dziel = odejmij/dodaj;
		int modulo = 7%14;
		
		//operatory logiczne
		// && - operator and (mno�enie logiczne); daje prawd� gdy dwie ��czone operacje logiczne b�d� prawdziwe
		// || - operator or (suma logiczna); daje prawd� gdy jedna z ��czonych operacji logicznych b�dzie prawdziwa
		// < - operator mniejszo�ci; daje prawd� gdy warto�c po lewej stronie jest mniejsza od warto�ci po drugiej stronie
		// > - operator wi�kszo�ci; daje prawd� gdy warto�� po lewej stronie jest wi�ksza od warto�ci po prawej stronie
		// <= - operator mniejsze b�dz r�wne; dzia�a jak operator mniejszo�ci + daje prawd� gdy obie warto�ci maj� tak� sam� warto��
		// >= - operator wi�ksze b�dz r�wne; dzia�a jak operator wi�kszo�ci + daje prawd� gdy obie warto�ci maj� tak� sam� warto��
		// == - operator r�wno�ci; daje prawd� gdy oba por�wnywane argumenty  maj� tak� sam� warto�� 
		// != - operator nier�wno�ci; daje prawd� gdy oba por�wnywane argumenty maj� r�ne warto�ci
		// ! - operator negacji; odrwaca warto�� logiczn� (false -> true; true -> false)
		
		//przyk�ady
		boolean mniejsze1 = 5<7; //prawda
		boolean mniejsze2 = 7<3; //fa�sz
		
		boolean wieksze1 = 7>3; //prawda
		boolean wieksze2 = 7>140; //fa�sz
		
		boolean mniejszerowne1 = 7<=7; //prawda
		boolean mniejszerowne2 = 7<=6; //fa�sz
		
		boolean wiekszerowne1 = 8>=8; //prawda
		boolean wiekszerowne2 = 8>=7;//fa�sz
		
		boolean rowne1 = 89==89;//prawda
		boolean rowne2 = 89==102;//fa�sz
		
		boolean rozne1 = 90!=91; //prawa
		boolean rozne2 = 90!=90;//fa�sz
		
		boolean odwroc = !rozne1;//fa�sz
		
		boolean and1 = mniejsze1 && mniejszerowne1; //prawda;
		boolean and2 = mniejsze1 && odwroc; //fa�sz
		
		boolean or1 = rozne2 || odwroc; //fa��z
		boolean or2 = rozne1 || odwroc;//prawda
		
//		System.out.println(mniejsze1);
//		System.out.println(mniejsze2);
//		System.out.println(mniejszerowne1);
//		System.out.println(mniejszerowne2);
//		System.out.println(odwroc);
		
		//operatory bitowe
		// & - operator bitowego mno�enia (and) 
		// | - opertor bitowej sumy (or)
		// << - operator przesuni�cia bit�w w lewo
		// >> - operator przesuniecia bitow w prawo
		// ^ - operacja exor
		// ~ - operacja negacji (odwr�cenie warto�ci)
		
		int band = 10&2;
		int bor = 10|3;
		int blshift = 1 << 4;
		int brshift = 1024 >> 4;
		int bexor = 9^3;
		int bneg = ~37;
		
//		//   1010
//		//   0010 AND
//		//=  0010
//		System.out.println(band);
//		
//		//  1010
//		//  1011
//		//= 1011 OR
//		System.out.println(bor);
//		
//		//   00001
//		// przesuwamy bit z 1 o 4 miejsca w lewo
//		//=  10000
//		System.out.println(blshift);
//		
//		//  10000000000
//		// przeni�cie bitu o 4 miejsca w prawo
//		//= 00001000000
//		System.out.println(brshift);
//		
//		//  1001
//		//  0011
//		//= 1010 ExOR
//		System.out.println(bexor);
//		
//		//  00000000000000000000000000100101
//		//  negujemy warto�� KA�DEGO BITU
//		//= 11111111111111111111111111011010
//		System.out.println(bneg);
		
		//jakie s� jeszcze operatory? Jak mo�na je wykorzysta�
		/*
		 * Poni�szy kod ma na celu przybli�enie dzia�ania instrukcji warunkowych
		 * oraz instrukcji steruj�cych (p�tli)
		 */
		
		int nowaLiczba=0;
		System.out.println("Podaj dowoln� liczb� ca�kowit�: ");
		Scanner sc = new Scanner(System.in);
		nowaLiczba = sc.nextInt();
		
		/* Piersza, podstawowa konstrukcja warunk�w w programowaniu. Kod wykona si� w klauzuli if je�eli
		 * warunek podany w nawiasie okr�g�ym da wynik true (prawda). W przeciwnym razie kod nie zostanie
		 * wykonany.
		 * Oryginalnie warunek posiada sk�adni�
		 * 
		 * if (warunek) { kod, gdy zostanie spe�niony warunek}
		 * 
		 * mo�na jednak poszerzy� dzia�anie warunku o klauzul� else
		 * 
		 * if (warunek) { kod, gdy zostanie spe�niony warunek}
		 * else { kod, gdy warunek nie zosta� spe�niony }
		 * 
		 * w tym wypadku warunek wykona si� zawsze (inaczej dla spe�nionego warunku,
		 * inaczej gdy warunek nie zostanie spe�niony)
		 * 
		 *  Warunki mo�na ��czy� (tworz�c blok decyzyjny) poprzez do��czanie
		 *  kolejnego warunku (if) po s�owie else
		 *  
		 *  
		 * Zasada dzia�ania bloku - ZAWSZE wykonywany jest pierwszy napotkany spe�niony warunek
		 * za� pozosta�e si� nie wykonaj�!
		 */
		if (nowaLiczba>0) {
			System.out.println("Podana przez Ciebie liczba jest dodatnia.");
		}
		//je�eli chcemy rozpoznawa�, �e liczba jest dodatani i DODATKOWO
		//sprawdza�, czy warto�� jest wi�ksza od 150 to w linii ni�ej 
		//nale�y usun�� s�owo else (rozdzielimy tym blok if i otrzymamy
		//dwa bloki decyzyjne)
		else if (nowaLiczba>150) {
			System.out.println("Podana przez Ciebie liczba jest du�a.");
		}
		else if (nowaLiczba<30) {
			System.out.println("Podana liczba jest ma�a.");
		}
		else {
			System.out.println("Poda�e� normaln� liczb�.");
		}
		System.out.println("Program ko�czy dzia�anie.");
	}

}