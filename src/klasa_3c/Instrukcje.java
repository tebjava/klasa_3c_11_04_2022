package klasa_3c;

import java.util.Scanner;

public class Instrukcje {
	public static void main(String[] args) {
		System.out.print("Podaj dowoln� warto�� z przedzia�u 1-100: ");
		Scanner sc = new Scanner(System.in);
		int liczba = sc.nextInt();
		
		/*
		 * Inkrementajca i dekrementacja w C/C++ i innych j�zykach
		 * 
		 * Poj�cie inkrementacji kryje w sobie mo�liwo�� zwi�kszania
		 * wskazanej warto�ci zapisanej pod okre�lon� zmienn� dok�adnie o 
		 * jedn� warto�� (np. z 2 na 3, z 3 na 4 itd.)
		 * 
		 * Wyst�puj� dwa rodzaje inkrementacji:
		 * - preinkrementacja - ten rodzaj operacji zmienia warto�� obliczanej
		 * wielko�ci PRZED wykonaniem si� linii kodu, w kt�rym u�ytko operatora
		 * inkrementacji.
		 * - postinkrementacja -ten sam rodzaj operacji (zw�kszanie obecnej warto�ci
		 * o jeden) jednak zmiana dokonuj� si� po wykonaniu linii kodu,
		 * w kt�rej znajduje si� operator.
		 * 
		 * Przyk�ad:
		 * int a = 0;
		 * cout << a++; //wy�wietlona warto�� to nadal zero; warto�� a zostanie zmieniona dopiero
		 *              //wykonaniu wy�wietlenia zawarto�ci a na konsoli systemowej
		 * cout << ++a; //wy�wietlona warto�� to 2; warto�c a po pierwszej linii posiada�a jeden
		 *             //jednak poniewa� plusy s� przed zmienn� to nakazujemy zmian� warto�ci
		 *             //przed wykonaniem ca�ej linii kodu.
		 *           
		 * cout << --a; //predekrementacja
		 * cout << a--; //postinkrementacja
		 */
		
		while(--liczba>0) {
			System.out.println("Zosta�o jeszcze " + liczba + " z liczby");
		}
		
		/*
		 * P�tla for jest jedn� z najpopularniejszych i najefektywniejszych p�tli dost�pnych w programowaniu
		 * Pozwala ona bowiem na wykonanie okre�lonego kodu tak d�ugo, jak spe�nia si� podany w niej warunek.
		 * P�tla pozwala na swoim stracie zdefiniowa� parametr kontroluj�cy p�tl�, pozwala zdefiniowa�
		 * warunek, kt�ry musi by� spe�niany oraz pozwala na zdefiniowanie operacji, co ma si� wykonywa�
		 * po tym, jak p�tla zako�czy sw�j blok i wr�ci do pierwszej lini.
		 * 
		 * Klasycznie p�tla for zawiera taki oto zestaw parametr�w:
		 * for (int i=0;i<10;i++)
		 * 
		 * pierwszy element w nawiasie okr�g�ym to zdefiniowanie warto�ci zadeklarowanej w tym samym miejscu
		 * zmiennej, kt�ra mo�e przyj�� dowoln� warto��, jednak przewa�nie ustawia si� jej warto�� 0. Warto�� ta
		 * jest wykorzystywana do iterowania (przemieszczanie si� warto�� po warto�ci przez "ci�g" liczbowy)
		 * przez kolejne warto�ci ca�kowite do przodu (dodawanie) lub do ty�u (odejmowanie). Warto�� zdefiniowanego
		 * i mo�e by� dowolnie modyfikowana
		 * 
		 * drugi element to warunek, jaki musi by� spe�niony by p�tla funkcjonowa�a. Mo�e to by� prosty warunek
		 * (np. i<10) jak te� bardziej skomplikowany (np. i<60 && i > 40).
		 * 
		 *  ostatni element to dzia�anie, jakie ma zosta� wykonane po tym, gdy p�tla przjedzie wszystkie instrukcje
		 *  w jej bloku kodu. Najcz�ciej jest to zwi�kszanie warto�ci wcze�niej zdefiniowanej zmiennej o 1, aczkolwiek
		 *  mog� to by� dowolne dzia�ania.
		 *  
		 *  Poni�ej przyk�adowa p�tla posiadaj�ca zdefiniowane wi�cej ni� jedn� zmienn� (dok�adnie posiada 
		 *  zdefiniowane dwie zmienne - i oraz a, kt�re otrzymuj� okre�lone warto�ci pocz�tkowe).
		 *  Nast�pnie warunek jest logicznie po��czony (i musi by� wi�ksze od zera oraz a musi by� mniejse od
		 *  czterech)
		 *  Na koniec, po przecinku jedna z warto�ci jest zwi�kszana o 1, za kolejna jest zmniejszana o 1
		 *  UWAGA! To czy u�yjemy preinkrementacji/dekrementacji czy postinkrementacji/dekrementacji
		 *  nie ma mniejszego znacznie w przypadku p�tli for - zawsze dostaniemy ten sam wynik! 
		 */
		
		for (int i=0,a=11;i>0 && a<4;i++,a--) {
			
		}
		
//		System.out.println("If-else if-else:");
//		if(liczba==0) {
//			System.out.println("Jak ka�dy programista - zaczynasz od zera");
//		}
//		else if (liczba<20) {
//			System.out.println("Minimalista");
//		}
//		else if (liczba<50) {
//			System.out.println("Nadal ma�o");
//		}
//		else if (liczba<80) {
//			System.out.println("W sam raz");
//		}
//		else if (liczba==100) {
//			System.out.println("Uwielbiasz maksimum");
//		}
//		
//		else {
//			System.out.println("Nie potrafisz si� bawi�.");
//		}
//		
//		System.out.println("Switch-case:");
//		switch (liczba) {
//			case 100: System.out.println("Uwielbiasz maksimum"); 
//					  break;
//			case 80: System.out.println("W sam raz"); 
//			         break;
//			case 50: System.out.println("Nadal ma�o"); 
//			         break;
//			case 20: System.out.println("Minimalista"); 
//			         break;
//			case 0: System.out.println("Jak ka�dy programista - zaczynasz od zera"); 
//			        break;
//			default: System.out.println("Nie potrafisz si� bawi�.");
//		}
		
		sc.close();
	}
}
